/* 
Escreva um algoritmo para ler uma temperatura em graus Fahrenheit,
calcular e escrever o valor correspondente em graus Celsius (baseado na
fórmula abaixo):

𝐶/5 = ( 𝑓 − 32)/9
*/

function celsius(fahrenheit) {
    var celsius = ((fahrenheit - 32) / 9) * 5;
    return celsius.toFixed(2);
}

var rand = (Math.random() * 100).toFixed(2);

celsius = celsius(rand);

console.log(`${rand} graus Fahrenheits = ${celsius} graus Celsius`);
