/* 
Faça um algoritmo em que você recebe 3 notas de um aluno e caso a
média aritmética dessas notas for maior ou igual que 6 imprima
“Aprovado”, caso contrário “Reprovado”
*/
function media(n1, n2, n3) {
    sum = (n1 + n2 + n3) / 3;
    return sum;
} 

function situacao(media){
    if (media >= 6) {
        console.log("Aprovado");
    } else {
        console.log("Reprovado");
    }
}

let n1 = Math.floor(Math.random() * 10);
let n2 = Math.floor(Math.random() * 10);
let n3 = Math.floor(Math.random() * 10);
let media_ = (media(n1, n2, n3)).toFixed(1);

console.log(`Notas:\n${n1}\n${n2}\n${n3}\n\nMédia: ${media_}\n`);
situacao(media_);
