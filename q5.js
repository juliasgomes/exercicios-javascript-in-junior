/* 
5. Teste 5 números inteiros aleatórios. Os testes:
● Caso o valor seja divisível por 3, imprima no console “fizz”.
● Caso o valor seja divisível por 5 imprima “buzz”.
● Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima
“fizzbuzz”.
● Caso contrário imprima nada 
*/
function fizzbuzz(valor) {
    if (valor % 3 == 0 && valor % 5 == 0) {
        return 'fizzbuzz';
    } else if (valor % 3 == 0) {
        return 'fizz';
    } else if (valor % 5 == 0) {
        return 'buzz';
    } else {
        return '';
    }
}

function rand() {
    return Math.floor(Math.random() * 100)
}

var i = 0

for (i; i < 5; i++){
    random = rand();
    console.log(`${random} -> ${fizzbuzz(random)}`);
}