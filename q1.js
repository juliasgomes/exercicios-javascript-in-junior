/* 
Implemente um algoritmo que pegue duas matrizes (array de arrays) e realize sua multiplicaçao.
Lembrando que para realizar a multiplicação dessas matrizes o número de colunas da primeira
matriz tem que ser igual o número de linhas da segunda matriz.
*/

let matriz1 =  [[2,-1],[2,0]]
let matriz2 = [[2,3],[-2,1]]

function mult_matriz(m1, m2){
    colunasM1 = m1[0].length 
    linhasM2 = m2.length
    let matriz3 = [["",""],["",""]]
    
    if(colunasM1 == linhasM2){
        for(i = 0; i < 2; i++) {
            for(j = 0; j < 2; j++) {
                let soma = 0;
                for(k =0; k < 2; k = k + 1) {
                     soma = soma + matriz1[i][k] * matriz2[k][j];
                     matriz3[i][j] = soma;
                }
            }
        }
    }
    return matriz3;
}

let matriz3 = mult_matriz(matriz1, matriz2);
console.log(matriz3);